﻿using System;

namespace Section5ControlFlow1
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Write a program and ask the user to enter a number.
             *The number should be between 1 to 10.
             *If the user enters a valid number, display "Valid" on the console.
             *Otherwise, display "Invalid".*/

            Console.Write("Enter a number number from 1 to 10: ");
            int number = int.Parse(Console.ReadLine());

            if (number > 1 && number < 11)
            {
                Console.WriteLine("\nValid");
            }
            else
            {
                Console.WriteLine("\nInvalid");
            }

            Console.WriteLine("\n--------------------------------------------------------------\n");

            /*Write a program which takes two numbers from the
             *console and displays the maximum of the two*/

            Console.Write("Type the first number: ");
            int firstNumber = int.Parse(Console.ReadLine());
            Console.Write("Type the second number: ");
            int secondtNumber = int.Parse(Console.ReadLine());

            var maxNumber = firstNumber > secondtNumber ? firstNumber : secondtNumber;

            Console.WriteLine($"\n{maxNumber} is the bigger number");

            Console.WriteLine("\n--------------------------------------------------------------\n");


            /*Write a program and ask the user to enter the width and height of an image. 
             *Then tell if the image is landscape or portrait.*/

            Console.Write("Enter the width of an image: ");
            int imageWidth = int.Parse(Console.ReadLine());
            Console.Write("Enter the Height ");
            int imageHeight = int.Parse(Console.ReadLine());

            var imageOrientation = imageWidth > imageHeight ? "\nLandscape" : "\nPortrait";

            Console.WriteLine(imageOrientation);

            Console.WriteLine("\n--------------------------------------------------------------\n");

            /*Your job is to write a program for a speed camera. 
             *For simplicity, ignore the details such as camera, sensors, etc and focus purely on the logic. 
             *Write a program that asks the user to enter the speed limit. 
             *Once set, the program asks for the speed of a car. 
             *If the user enters a value less than the speed limit, program should display Ok on the console. 
             *If the value is above the speed limit, the program should calculate the number of demerit points. 
             *For every 5km/hr above the speed limit, 1 demerit points should be incurred and displayed on the console. 
             *If the number of demerit points is above 12, the program should display License Suspended.*/

            Console.WriteLine("What is the speed limit?");
            float speedLimit = float.Parse(Console.ReadLine());

            Console.WriteLine("What was the speed of the car?");
            float carSpeed = float.Parse(Console.ReadLine());

            if (carSpeed <= speedLimit)
                Console.WriteLine("Ok!");
            if (carSpeed > speedLimit)
            {
                var speedDifference = (carSpeed - speedLimit);
                int pointsLost = ((int)(speedDifference / 5));
                if (pointsLost <= 12)
                {
                    Console.WriteLine($"\nYou were driving above the speed limit! You lose {pointsLost} point(s)");
                }
                else
                {
                    Console.WriteLine("\nLicense Suspended".ToUpper());
                }
            }

            Console.WriteLine("\n--------------------------------------------------------------\n");

        }
    }
}
